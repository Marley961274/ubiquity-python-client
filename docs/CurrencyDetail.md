# CurrencyDetail


## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**type** | **str** | Name of token mechanism (smart contract) | [optional] 
**id** | **str** | Token identifier | [optional] 
**creator** | **str** | Address that created token | [optional] 
**contract** | **str** | Address of contract | [optional] 
**any string name** | **bool, date, datetime, dict, float, int, list, str, none_type** | any string name can be used but the value must be the correct type | [optional]

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


