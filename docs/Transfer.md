# Transfer

Transfer of currency from one account to another

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**_from** | **str** | Sender address | 
**to** | **str** | Receiver address | 
**currency** | [**Currency**](Currency.md) |  | 
**value** | **str** | Integer string in smallest unit (Satoshis) | 
**fee** | [**Fee**](Fee.md) |  | [optional] 
**any string name** | **bool, date, datetime, dict, float, int, list, str, none_type** | any string name can be used but the value must be the correct type | [optional]

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


